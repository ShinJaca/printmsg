#include <stdio.h>
#include <stdlib.h>

#define C_RST "\x1b[m"
#define C_BGREEN "\x1b[1;30;42m"

#define M_OK    "\x1b[1;30;42m  OK  \x1b[m"
#define M_FAIL  "\x1b[1;30;45m FAIL \x1b[m"


int main(int argc, char const *argv[])
{
    if (strcmp(argv[1], "ok")==0)
    {
        printf("%s %s\n", M_OK, argv[2]);
    }
    if (strcmp(argv[1], "fail")==0)
    {
        printf("%s %s\n", M_FAIL, argv[2]);
    }
    return 0;
}
