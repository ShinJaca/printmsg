# Name of the project
PROJ_NAME=pmsg
VERSION=0.0-1
ARQ=amd64

# Package Information
MAINTAINER=ShinJaca <sergiopugli@gmail.com>
SHORT_DESC=Prints a message and a status flag
LONG_DESC=	Prints a message specified by the user and a colored status flag at the beggining of the line

define PACK_TEXT
Package: $(PROJ_NAME)
Version: $(VERSION)
Architecture: $(ARQ)
Maintainer: $(MAINTAINER)
Description: $(SHORT_DESC)
	$(LONG_DESC)
endef

# .c files
C_SOURCE=$(wildcard ./src/*.c)
 
# .h files
H_SOURCE=$(wildcard ./src/*.h)
 
# Object files
OBJ=$(subst .c,.o,$(subst src,obj,$(C_SOURCE)))
 
# Compiler and linker
CC=gcc
 
# Flags for compiler
CC_FLAGS=-c         \
         -W         \
         -Wall      \
         -ansi      \
         -pedantic
 
# Command used at clean target
RM = rm -rf
 
#
# Compilation and linking
#
all: objFolder $(PROJ_NAME)
 
$(PROJ_NAME): $(OBJ)
	@ echo 'Building binary using GCC linker: $@'
	$(CC) $^ -o $@
	@ echo 'Finished building binary: $@'
	@ echo ' '
 
./obj/%.o: ./src/%.c ./src/%.h
	@ echo 'Building target using GCC compiler: $<'
	$(CC) $< $(CC_FLAGS) -o $@
	@ echo ' '
 
./obj/main.o: ./src/main.c $(H_SOURCE)
	@ echo 'Building target using GCC compiler: $<'
	$(CC) $< $(CC_FLAGS) -o $@
	@ echo ' '
 
objFolder:
	@ mkdir -p obj

pkgFolder:
	@ mkdir -p dist/$(PROJ_NAME)_$(VERSION)_$(ARQ)/DEBIAN

destFolder:
	@ mkdir -p dist/$(PROJ_NAME)_$(VERSION)_$(ARQ)/usr/bin

 
clean:
	@ $(RM) ./obj/*.o $(PROJ_NAME) *~
	@ rmdir obj

control: pkgFolder destFolder
	$(file >./dist/$(PROJ_NAME)_$(VERSION)_$(ARQ)/DEBIAN/$@,$(PACK_TEXT))

package: control
	cp $(PROJ_NAME) ./dist/$(PROJ_NAME)_$(VERSION)_$(ARQ)/usr/bin/
	dpkg-deb --build --root-owner-group ./dist/$(PROJ_NAME)_$(VERSION)_$(ARQ)
 
.PHONY: all clean package
